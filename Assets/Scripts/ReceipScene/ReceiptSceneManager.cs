﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace ReceiptScene
{
    public enum ReceiptSceneState
    {
        None,
        ShowingReceipt,
        ReceiptHiden,
        PrepareToShowNewRecept
    }
    public enum Difficult
    {
        Low,
        Medium,
        Hard
    }
    public class ReceiptSceneManager : MonoBehaviour
    {
        #region Editor fields
        [Header("Иконки для продуктов")]
        [SerializeField] private List<Image> iconHolders = new List<Image>();

        [Header("Список рецептов")]
        [SerializeField] private List<Receipt> receipts = new List<Receipt>();

        [Header("Кнопка 'Дальше'")]
        [SerializeField] private GameObject btnNext;

        [Header("Текст кнопки")]
        [SerializeField] private Text btnNextText;

        [Header("Текст рецепта")]
        [SerializeField] private Text receiptText;

        [Header("Текст имени рецепта")]
        [SerializeField] private Text receiptNameText;

        [Header("Задержка скрытия рецепта")]
        [SerializeField] private float hideDelay;

        [Header("Иконка кнопки 'Легкий'")]
        [SerializeField]
        private Image lowBtnIcon;
        [Header("Иконка кнопки 'Средний'")]
        [SerializeField]
        private Image mediumBtnIcon;
        [Header("Иконка кнопки 'Сложный'")]
        [SerializeField]
        private Image harBtnIcon;
        [Header("Цвет выбранной сложности")]
        [SerializeField]
        private Color SelectedColor;
        [SerializeField]
        private Color oldColor;
        #endregion

        #region private variables
        private List<int> availableReceipIndexes = new List<int>();
        private Receipt currentReceipt = null;
        private ReceiptSceneState currentState = ReceiptSceneState.None;
        private Difficult currentDifficult = Difficult.Low;
        #endregion

        #region public variables

        #endregion

        #region Unity Methods
        private void Awake()
        {
            CreateReceiptList();
            foreach (Image img in iconHolders)
            {
                img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
            }
        }
        private void Start()
        {
            UpdateButtons();
            if(currentDifficult!=Difficult.Low)
            btnNext.SetActive(false);
            ShowNextReceipt();
        }
        #endregion

        #region Private Methods
        private void CreateReceiptList()
        {
            availableReceipIndexes.Clear();
            for (int i = 0; i < receipts.Count; i++) availableReceipIndexes.Add(i);
        }

        private bool SelectRandomIndex(List<int> list, out int index)
        {
            index = -1;
            
            if (list.Count > 0)
            {
                if (list.Count==1)
                {
                    index = list[0];
                    return true;
                }
                int id = UnityEngine.Random.Range(0, list.Count);
                index = list[id];
                return true;

            }
            return false;
        }
        private void HideReceipt()
        {
            receiptText.gameObject.SetActive(false);
            foreach(Image img in iconHolders)
            {
                img.gameObject.SetActive(false);
            }
            btnNext.SetActive(true);
            btnNextText.text = "Показать рецепт";
            currentState = ReceiptSceneState.ReceiptHiden;
        }
        private void ShowCurrentReceipt()
        {
            receiptText.gameObject.SetActive(true);
            for (int i = 0; i < iconHolders.Count; i++)
            {
                iconHolders[i].gameObject.SetActive(true);
                if (i < currentReceipt.ingridients.Count)
                {                    
                    iconHolders[i].color = new Color(iconHolders[i].color.r, iconHolders[i].color.g, iconHolders[i].color.b, 1);
                    iconHolders[i].sprite = currentReceipt.ingridients[i].icon;
                }
                else
                {
                    iconHolders[i].color = new Color(iconHolders[i].color.r, iconHolders[i].color.g, iconHolders[i].color.b, 0);
                }
            }
            btnNextText.text = "Следующий рецепт";
            currentState = ReceiptSceneState.PrepareToShowNewRecept;
        }
        private void DisplayCurrentReceipt()
        {
            currentState = ReceiptSceneState.ShowingReceipt;
            receiptText.text = "";
            if (currentDifficult != Difficult.Low) btnNext.SetActive(false);
            receiptText.gameObject.SetActive(true);
            receiptNameText.text = currentReceipt.receiptName;
            for (int i=0;i< iconHolders.Count;i++)
            {
                if(i< currentReceipt.ingridients.Count)
                {
                    if (currentDifficult == Difficult.Hard)
                    {
                        iconHolders[i].color = new Color(iconHolders[i].color.r, iconHolders[i].color.g, iconHolders[i].color.b, 0);
                    }
                    else
                    {
                        iconHolders[i].gameObject.SetActive(true);
                        iconHolders[i].color = new Color(iconHolders[i].color.r, iconHolders[i].color.g, iconHolders[i].color.b, 1);
                        iconHolders[i].sprite = currentReceipt.ingridients[i].icon;
                    }
                    receiptText.text = receiptText.text + "\n" + currentReceipt.ingridients[i].name;
                }
                else
                {
                    iconHolders[i].color = new Color(iconHolders[i].color.r, iconHolders[i].color.g, iconHolders[i].color.b, 0);
                }
            }
        }
        private void ShowNextReceipt()
        {
            int receiptID = -1;
            if (availableReceipIndexes.Count > 0)
            {
                if (SelectRandomIndex(availableReceipIndexes, out receiptID))
                {
                    if (receiptID >= 0)
                    {
                        currentReceipt = receipts[receiptID];
                        availableReceipIndexes.Remove(receiptID);
                        DisplayCurrentReceipt();
                        switch(currentDifficult)
                        {
                            case Difficult.Low:
                                break;
                            case Difficult.Medium:
                                Invoke("HideReceipt", hideDelay);
                                break;
                            case Difficult.Hard:
                                Invoke("HideReceipt", hideDelay);
                                break;
                        }                        
                    }
                }
            }
            else
            {
                CreateReceiptList();
                ShowNextReceipt();
            }
        }
        private void UpdateButtons()
        {
            switch (currentDifficult)
            {
                case Difficult.Low:                   
                    lowBtnIcon.color = SelectedColor;
                    mediumBtnIcon.color = oldColor;
                    harBtnIcon.color = oldColor;
                    break;
                case Difficult.Medium:
                    lowBtnIcon.color = oldColor;
                    mediumBtnIcon.color = SelectedColor;
                    harBtnIcon.color = oldColor;                    
                    break;
                case Difficult.Hard:
                    lowBtnIcon.color = oldColor;
                    mediumBtnIcon.color = oldColor;
                    harBtnIcon.color = SelectedColor;                    
                    break;
            }
            ShowNextReceipt();

        }
        #endregion

        #region Public Methods
        public void GoMenu()
        {
            SceneManager.LoadScene(0);
        }
        public void OnNextBtnClick()
        {
            StopAllCoroutines();
            switch(currentState)
            {
                case ReceiptSceneState.None:
                    break;
                case ReceiptSceneState.PrepareToShowNewRecept:
                    ShowNextReceipt();
                    break;
                case ReceiptSceneState.ReceiptHiden:
                    ShowCurrentReceipt();
                    break;
                case ReceiptSceneState.ShowingReceipt:
                    ShowNextReceipt();
                    break;
            }
        }
        public void OnLow()
        {
            currentDifficult = Difficult.Low;
            UpdateButtons();
        }
        public void OnMedium()
        {
            currentDifficult = Difficult.Medium;
            UpdateButtons();
        }
        public void OnHard()
        {
            currentDifficult = Difficult.Hard;
            UpdateButtons();
        }
        #endregion
    }
}