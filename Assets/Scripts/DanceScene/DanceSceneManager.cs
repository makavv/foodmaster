﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
namespace DanceScene
{
    public enum PlayState
    {
        None,
        PlayVideo,
        PlayAudio,
        Finished
    }
    public class DanceSceneManager : MonoBehaviour
    {
        #region Editor fields
        [Header("Видео клипы")]
        [SerializeField] private List<VideoClip> videoClips = new List<VideoClip>();
        [Header("Музыка")]
        [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();

        [SerializeField] private VideoPlayer videoPlayer;
        [SerializeField] private AudioSource audioPlayer;

        [Header("Изображение под видео")]
        [SerializeField] private RawImage videoImage;
        
        [Header("Задержка между видео")]
        [SerializeField] private float videoDelay;
        [Header("Задержка между музыкой")]
        [SerializeField] private float audioDelay;
        [Header("Изображение для воспроизведения с вебки")]
        [SerializeField] private RawImage webCamRawImage;
        [Header("Ширина кадра вебкамеры")]
        [SerializeField] private int webCamWidth=1280;
        [Header("Высота кадра вебкамеры")]
        [SerializeField] private int webCamHeight=720;
        [Header("Частота кадров вебки")]
        [SerializeField] private int webCamFPS=25;
        
        #endregion

        #region private variables
        private List<int> videoPlayList = new List<int>();
        private List<int> audioPlayList = new List<int>();
        private WebCamTexture webCamTexture = null;
        private string deviceName;
        #endregion

        #region public variables

        #endregion

        #region Unity Methods
        private void Awake()
        {
            for (int i = 0; i < videoClips.Count; i++) videoPlayList.Add(i);
            for (int i = 0; i < audioClips.Count; i++) audioPlayList.Add(i);
        }
        private void Start()
        {
            WebCamDevice[] devices = WebCamTexture.devices;
            if (devices.Length > 0)
            {
                int camIndex = PlayerPrefs.GetInt("WebCam", 0);
                deviceName = devices[camIndex].name;
                webCamTexture = new WebCamTexture(deviceName, webCamWidth, webCamHeight, webCamFPS);
                webCamRawImage.texture = webCamTexture;
                webCamTexture.Play();
            }
            else
            {
                Debug.Log("Error! No webcam devices!");
            }
            PlayNextVideo();            
        }
        #endregion

        #region Private Methods
        private void OnDestroy()
        {
            webCamTexture.Stop();
            Destroy(webCamTexture);
        }
        private void PlayNextVideo()
        {
            int videoID = -1;
            if(SelectRandomIndex(videoPlayList, out videoID))
            {                
                if (videoID >= 0 && videoPlayer)
                {
                    videoPlayList.Remove(videoID);                  
                    videoPlayer.clip = videoClips[videoID];
                    videoPlayer.Play();                    
                    Invoke("PlayNextVideo", (float)(videoPlayer.clip.length + videoDelay));
                }
            }
            else
            {
                videoPlayer.Stop();
                videoImage.gameObject.SetActive(false);
                PlayNextAudio();
            }
            
        }
        private void PlayNextAudio()
        {
            int musicID = -1;
            if (audioPlayList.Count > 0)
            {
                if (SelectRandomIndex(audioPlayList, out musicID))
                {
                    if (musicID >= 0 && videoPlayer)
                    {
                        audioPlayList.Remove(musicID);
                        audioPlayer.clip = audioClips[musicID];
                        audioPlayer.Play();
                        Invoke("PlayNextAudio", (float)(audioPlayer.clip.length + audioDelay));
                    }
                }
            }
            else
            {
                Debug.Log("Scene ended!");
            }
        }
        private bool SelectRandomIndex(List<int> list, out int index)
        {           
            index = -1;           
            if (list.Count > 0)
            {
                int id = UnityEngine.Random.Range(0, list.Count);
                index = list[id];
                return true;
            }
            return false;
        }
        #endregion

        #region Public Methods
        public void GoMenu()
        {
            SceneManager.LoadScene(0);
        }
        #endregion
    }
}