﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindSets : MonoBehaviour
{
    int lifes;
    [Space]
    public List<SetCards> enemiesWaves = new List<SetCards>()
{
    new SetCards(),
    new SetCards(),
    new SetCards(),
};

    [Serializable]
    public class SetCards
    {
       
        [TableList(DrawScrollView = true, MaxScrollViewHeight = 600, MinScrollViewHeight = 400)]

        public List<Card> Card;

    }
    [TableList(ShowIndexLabels = true)]
    [Serializable]
    public class Card
    {
        [TableColumnWidth(100, Resizable = false), VerticalGroup("Свойство")]
        [LabelWidth(80), GUIColor(1, 0.2f, 0)]
        public bool negativeSprite = false;

        [ VerticalGroup("Свойство")]
        [PreviewField(Alignment = ObjectFieldAlignment.Center)]
        public Texture Icon;

        // [HideIf("negative")]
        [ VerticalGroup("Цвет")]
        [LabelWidth(80),GUIColor(1, 0.2f,0)]
        public bool negativeColor = false;
        [ VerticalGroup("Цвет")]
        [ColorPalette("MainColors")]
        public Color cardColor;



    }




    }
