﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using EpPathFinding.cs;

namespace HideAndSeekScene
{
    [System.Serializable]
    public class GridCellContainer
    {
        public Vector2 position;        
        public GridPos gridPosition;
    }
    [System.Serializable]
    public struct GridParams
    {
        public int width, height;
        public float borderOffset;
        public int minWayDistance;
        public int maxWayPoints;
    }
    public enum Difficult
    {
        Low,
        Medium,
        Hard
    }
    public class HideAndSeek : MonoBehaviour
    {
        #region Editor fields
        [Header("Параметры сетки")]
        [SerializeField]
        private GridParams gridParams;

        [Header("Префаб еды")]
        [SerializeField] private GameObject foodContainerPrefab;

        [Header("Контейнер для еды")]
        [SerializeField] private RectTransform foodContainer;

        [Header("Список групп тарелок")]
        [SerializeField] private List<PlateGroup> plateGroups = new List<PlateGroup>();
        [Header("Задержка перед показом тарелок")]
        [SerializeField] private float  showDelay;
        [Header("Список доступных продуктов")]
        [SerializeField] private List<Sprite> foodSprites = new List<Sprite>();

        [Header("Панель поиска")]
        [SerializeField] private GameObject checkPanel;  
        [Header("Иконка искомого предмета")]
        [SerializeField] private Image itemToFindIcon;  

        [Header("Кнопка 'Следующий раунд'")]
        [SerializeField] private GameObject nextBtn;
        [Header("Кнопка 'Проверка'")]
        [SerializeField] private GameObject checkBtn;        
        [Header("Иконка кнопки 'Легкий'")]
        [SerializeField] private Image lowBtnIcon;
        [Header("Иконка кнопки 'Средний'")]
        [SerializeField] private Image mediumBtnIcon;
        [Header("Иконка кнопки 'Сложный'")]
        [SerializeField] private Image harBtnIcon;
        [Header("Цвет выбранной сложности")]
        [SerializeField] private Color SelectedColor;
        #endregion

        #region private variables
        private int currentFoodCount=0, maxFood=3, foodMoveEnded=0;
        private List<FoodMove> spawnedFood = new List<FoodMove>();
        private List<int> selectedFood = new List<int>();
        private List<int> selectedPlates = new List<int>();
        private List<int> selectedPos = new List<int>();
        private Vector2 LeftTop;
        private Vector2 RightBot;
        private List<GridCellContainer> cells = new List<GridCellContainer>();
        private BaseGrid levelGrid = null;
        private JumpPointParam wayRouter = null;
        private List<Vector2> availiablePositions = new List<Vector2>();
        private Difficult currentDifficult = Difficult.Low;
        private bool positionsCalced = false;
        private bool foodMoving = false;
        #endregion

        #region public variables
        public static HideAndSeek Instance = null;
        public System.Action GridCreated = null;
        public System.Action PositionsCalcEnded = null;
        public bool GridIsCreated { get; private set; }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                
                //DontDestroyOnLoad(gameObject);
            }
            nextBtn.SetActive(false);
            checkBtn.SetActive(false);
            checkPanel.SetActive(false);
            LeftTop = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height));
            LeftTop.Set(LeftTop.x+ gridParams.borderOffset, LeftTop.y- gridParams.borderOffset);
            RightBot = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0));
            RightBot.Set(RightBot.x - gridParams.borderOffset, RightBot.y + gridParams.borderOffset);
            Debug.Log(LeftTop);
            Debug.Log(RightBot);
            GridCreated += OnGridSpawned;
            PositionsCalcEnded += OnPositionsCalced;
        }
        private void Start()
        {
            UpdateDifficult();
            GenerateCellsGrid(gridParams.width, gridParams.height);
        }
        private void OnDrawGizmos()
        {
            /*Gizmos.color = Color.green;
            for (int i = 0; i < availiablePositions.Count; i++)
            {
               
                Gizmos.DrawSphere(ScreenToWorld(availiablePositions[i]), 0.2f);
            }*/
            Gizmos.color = Color.green;
            for (int i = 0; i < cells.Count; i++)
            {
                Gizmos.color = Color.green;
                if (!levelGrid.IsWalkableAt(cells[i].gridPosition)) Gizmos.color = Color.blue;
                Gizmos.DrawSphere(cells[i].position, 0.2f);
                
            }
            Gizmos.color = Color.gray;
        }
        #endregion

        #region Private Methods

        private void UpdateDifficult()
        {
            int plateCount = 3;
            switch(currentDifficult)
            {
                case Difficult.Low:
                    plateCount = 3;
                    lowBtnIcon.color = SelectedColor;
                    mediumBtnIcon.color = Color.white;
                    harBtnIcon.color = Color.white;
                    break;
                case Difficult.Medium:
                    lowBtnIcon.color = Color.white;
                    mediumBtnIcon.color = SelectedColor;
                    harBtnIcon.color = Color.white;
                    plateCount = 6;
                    break;
                case Difficult.Hard:
                    lowBtnIcon.color = Color.white;
                    mediumBtnIcon.color = Color.white;
                    harBtnIcon.color = SelectedColor;
                    plateCount = 9;
                    break;
            }
            maxFood = plateCount;
            int count = 0;
            foreach (PlateGroup pl in plateGroups)
            {
                foreach (Plate plate in pl.plates)
                {
                    if (count < plateCount) plate.gameObject.SetActive(true);
                    else plate.gameObject.SetActive(false);
                    count++;
                }
            }
        }
        private void GenerateCellsGrid(int width, int height)
        {
            levelGrid = new StaticGrid(width, height);
            float realWidth = Mathf.Abs(RightBot.x - LeftTop.x);
            float realHeight = Mathf.Abs(RightBot.y - LeftTop.y);

            float cellStep = realWidth / width;

            Vector2 startPos = new Vector2(LeftTop.x+ cellStep/2, RightBot.y+ cellStep/2);

            Vector2 spawnPosition = Vector2.zero;
            cells.Clear();
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    levelGrid.SetWalkableAt(i, j, true);
                    spawnPosition.Set(startPos.x+i* cellStep, startPos.y+j* cellStep);
                    GridCellContainer container = new GridCellContainer() { position = spawnPosition, gridPosition = new GridPos(i, j) };
                    cells.Add(container);
                }
            }
            
            wayRouter = new JumpPointParam(levelGrid, EndNodeUnWalkableTreatment.ALLOW, DiagonalMovement.OnlyWhenNoObstacles);
            GridIsCreated = true;
            GridCreated?.Invoke();
        }

        private IEnumerator CreatePositions()
        {
            yield return new WaitForEndOfFrame();
            availiablePositions.Clear();
            
            foreach (PlateGroup pl in plateGroups)
            {
                foreach(Plate plate in pl.plates)
                {
                    if(plate.gameObject.activeSelf) availiablePositions.Add(plate.rectTransform.anchoredPosition+new Vector2(0,Screen.height));                    
                }
            }
            positionsCalced = true;
            PositionsCalcEnded?.Invoke();
        }
        private void OnPositionsCalced()
        {
            if (GridIsCreated) StartGame();
        }
        private void OnGridSpawned()
        {
            StartCoroutine(CreatePositions());
        }
        private void GenerateWayForFood(FoodMove foodMove)
        {
            bool ok = false;
            GridCellContainer temp = null;
            while (!ok)
            {
                temp = FindRandomPos();
                if (IsPlaceFree(temp.gridPosition, foodMove.CellSize, foodMove.CellSize))
                {
                    foodMove.SetPosition(temp);
                    ok = true;
                    break;
                } 
            }
            
            for (int j = 0; j < gridParams.maxWayPoints; j++)
            {
                temp = FindRandomPosFrom(temp.gridPosition);
                foodMove.AddWayPoint(temp);
            }
            
            GridCellContainer endCell = null;
            Vector2 rndPlatePos = FindRandomAvailiablePlatePos();
            GetClosestGridCell(ScreenToWorld(rndPlatePos), out endCell);
            foodMove.SetPlate(endCell, rndPlatePos);
        }
        private void StartGame()
        {
            foodMoveEnded = 0;
            SetGridFree();
            if (currentFoodCount<maxFood)
            {
                selectedPos.Clear();
                selectedPlates.Clear();
                SpawnFood();
                FoodMove foodMove = null;
                for (int i = 0; i < (spawnedFood.Count-1); i++)
                {
                    foodMove = spawnedFood[i];
                    GenerateWayForFood(foodMove);
                    foodMove.StartMove();
                }
            }
            else
            {
                selectedPos.Clear();
                selectedFood.Clear();
                selectedPlates.Clear();
                FoodMove foodMove = null;
                for (int i = 0; i < spawnedFood.Count; i++)
                {
                    foodMove = spawnedFood[i];
                    Sprite icon = SelectRandomFoodIcon();
                    if(icon) foodMove.SetIconSprite(icon);
                    foodMove = spawnedFood[i];
                    GenerateWayForFood(foodMove);
                    foodMove.StartMove();
                }
            }
            
        }
        private void ShowFindPanel()
        {
            if (foodMoving) return;
            checkPanel.SetActive(true);
            int id = selectedFood[Random.Range(0, selectedFood.Count)];
            itemToFindIcon.sprite = foodSprites[id];
            checkBtn.SetActive(true);
        }
        private void ShowPlates()
        {
            if (foodMoving) return;
            foreach (PlateGroup pl in plateGroups)
            {
                foreach (Plate plate in pl.plates)
                {
                    if (plate.gameObject.activeSelf) plate.MoveToCenter();
                }
            }
            Invoke("ShowFindPanel", 1f);
        }
        private void OnFoodMoveEnded()
        {
            foodMoving = false;
            foodMoveEnded++;
            if(foodMoveEnded == currentFoodCount)
            {
                nextBtn.SetActive(false);
                
                Invoke("ShowPlates", showDelay);
            }
        }
        private Vector2 FindRandomAvailiablePlatePos()
        {
            bool ok = false;
            int index = 0;
            while (!ok)
            {
                index = Random.Range(0, availiablePositions.Count);
                if (!selectedPlates.Contains(index))
                {
                    selectedPlates.Add(index);
                    ok = true;
                    break;
                }
            }
            return availiablePositions[index];
        }
        private GridCellContainer FindRandomPos()
        {

            bool ok = false;
            int index = 0;
            
            while (!ok)
            {
                index = Random.Range(0, cells.Count);
                if (!selectedPos.Contains(index))
                {
                    selectedPos.Add(index);
                    ok = true;
                    break;                  
                }                
            }            
            return cells[index];
        }
        private GridCellContainer FindRandomPosFrom(GridPos pos)
        {

            bool ok = false;
            int index = 0;

            while (!ok)
            {
                index = Random.Range(0, cells.Count);
                if(Vector2.Distance(new Vector2(pos.x, pos.y),new Vector2(cells[index].gridPosition.x, cells[index].gridPosition.y))>=gridParams.minWayDistance)
                {
                    ok = true;
                    break;
                }
            }
            return cells[index];
        }
        private void SpawnFood()
        {
            currentFoodCount++;
            GameObject foodObj = Instantiate(foodContainerPrefab, foodContainer);
            FoodMove foodMove = null;
            if (foodObj.TryGetComponent(out foodMove))
            {
                foodMove.MoveEnded += OnFoodMoveEnded;
                spawnedFood.Add(foodMove);
                Sprite icon = SelectRandomFoodIcon();
                if (icon)
                {
                    foodMove.SetIconSprite(icon);
                    GenerateWayForFood(foodMove);
                    foodMove.StartMove();
                }
            }
        }
        private void SetGridFree()
        {
            for (int i = 0; i < cells.Count; i++) levelGrid.SetWalkableAt(cells[i].gridPosition, true);
        }
        private Sprite SelectRandomFoodIcon()
        {
            Sprite toRet = null;            
            bool ok = false;
            int iconID = 0;
            int iterCount = 0;
            while (!ok)
            {
                iconID = UnityEngine.Random.Range(0, foodSprites.Count);
                if (!selectedFood.Contains(iconID))
                {
                    selectedFood.Add(iconID);
                    toRet = foodSprites[iconID];
                    return toRet;                    
                }
                iterCount++;
                if (iterCount > 1000) ok = true;
            }
            return toRet;
        }
        private void Restart()
        {
            foodMoving = true;
            checkPanel.SetActive(false);
            foreach (PlateGroup pl in plateGroups)
            {
                foreach (Plate plate in pl.plates)
                {
                    if (plate.gameObject.activeSelf) plate.SetStartPos();
                }
            }
            nextBtn.SetActive(false);
            checkBtn.SetActive(false);
            currentFoodCount = 0;
            selectedFood.Clear();
            selectedPlates.Clear();
            selectedPos.Clear();
            foreach (FoodMove fm in spawnedFood)
            {
                Destroy(fm.gameObject);
            }
            spawnedFood.Clear();
            UpdateDifficult();
            StartCoroutine(CreatePositions());


        }
        #endregion

        #region Public Methods
        public List<GridCellContainer> FindWay(GridPos start, GridPos stop)
        {
            wayRouter.Reset(start, stop);
            List<GridPos> list = JumpPointFinder.FindPath(wayRouter);
            List<GridPos> full = JumpPointFinder.GetFullPath(list);
            List<GridCellContainer> way = new List<GridCellContainer>();
            GridCellContainer temp = null;
            for (int i=0;i<full.Count;i++)
            {
                if (GetGridCellContainer(full[i], out temp)) way.Add(temp);
            }

            return way;
        }
        public void GoMenu()
        {
            SceneManager.LoadScene(0);
        }
        public Vector2 ScreenToWorld(Vector2 pos)
        {
            return Camera.main.ScreenToWorldPoint(pos);//transf.TransformPoint(pos);
        }
        public Vector2 WorldToScreen(Vector2 pos)
        {
            return Camera.main.WorldToScreenPoint(pos);//transf.InverseTransformPoint(pos);
        }
        public bool GetGridCellContainer(GridPos pos, out GridCellContainer cell)
        {
            cell = null;
            for (int i = 0; i < cells.Count; i++)
            {
                if (pos == cells[i].gridPosition)
                {
                    cell = cells[i];
                    return true;
                }
            }
            return false;
        }

        public void SetCellOccupied(GridPos pos)
        {
            if (levelGrid != null)
            {
                levelGrid.SetWalkableAt(pos.x, pos.y, false);
            }
        }
        public void SetCellEmpty(GridPos pos)
        {
            if (levelGrid != null)
            {
                levelGrid.SetWalkableAt(pos.x, pos.y, true);
            }
        }
        public bool IsCellFree(GridPos pos)
        {
            if (levelGrid != null)
            {
                return levelGrid.IsWalkableAt(pos);
            }
            else return false;
        }
        public void SetPlacelWalkable(GridPos pos, int width, int height, bool walkable)
        {            
            int startX = pos.x - (int)(width/2);
            int startY = pos.y - (int)(height / 2);
            GridPos temPos = new GridPos();
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    temPos.Set(startX + i, startY + j);
                    if(temPos.x>= 0 && temPos.x< gridParams.width && temPos.y>= 0 && temPos.y< gridParams.height)
                    {
                        levelGrid.SetWalkableAt(temPos, walkable);
                    }
                    
                }
            }
        }
        public bool IsPlaceFree(GridPos pos, int width, int height)
        {
            if (!levelGrid.IsWalkableAt(pos)) return false;

            int startX = pos.x - (width - 1) / 2;
            int startY = pos.y - (height - 1) / 2;
            GridPos temPos = new GridPos();
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    temPos.Set(startX + i, startY + j);
                    if (!levelGrid.IsWalkableAt(temPos)) return false;
                }
            }
            return true;
        }
        public void GetClosestGridCell(Vector2 position, out GridCellContainer cell)
        {
            cell = null;
            float minDistance = float.MaxValue;
            float distance = 0;
            int index = 0;
            for (int i = 0; i < cells.Count; i++)
            {
                distance = Vector2.Distance(position, cells[i].position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    index = i;
                }
            }
            cell = cells[index];
        }
        public void Check()
        {
            checkPanel.SetActive(false);
            nextBtn.SetActive(true);
            checkBtn.SetActive(false);
            foreach (PlateGroup pl in plateGroups)
            {
                foreach (Plate plate in pl.plates)
                {
                    if (plate.gameObject.activeSelf) plate.MoveToStart();
                }
            }
        }
        public void NextRound()
        {
            nextBtn.SetActive(false);
            checkBtn.SetActive(false);
            StartGame();
        }
        
        public void OnLow()
        {                
            currentDifficult = Difficult.Low;
            Restart();
        }

        public void OnMedium()
        {            
            currentDifficult = Difficult.Medium;
            Restart();
        }

        public void OnHard()
        {            
            currentDifficult = Difficult.Hard;
            Restart();
        }
        #endregion
    }    
    [System.Serializable]
    public class PlateGroup
    {
        public List<Plate> plates = new List<Plate>();
        public int level = 0;
    }
}

/*
 * 
 #region Editor fields

 #endregion

 #region private variables

 #endregion

 #region public variables

 #endregion
 
 #region Unity Methods

 #endregion

 #region Private Methods

 #endregion

 #region Public Methods

 #endregion

 * */
