﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace QuestScene
{
    [CreateAssetMenu(fileName = "New Quest", menuName = "Quest data", order = 51)]
    public class Quest : ScriptableObject
    {
        public string questText;
        public string answerText;
        public Sprite answerIcon;
    }
}