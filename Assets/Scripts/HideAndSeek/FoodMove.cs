﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HideAndSeekScene
{
    [System.Serializable]
    public struct FoodMoveParams
    {
        [Header("Скорость движения еды")]        
        public float moveSpeed;
        [Header("Длительность случайного перемещения")]
        public float maxMovingTime;
        [Header("Размер в ячейках сетки")]
        public int cellSpace;
    }
    [System.Serializable]
    public enum FoodState
    {
        None,
        RandomMove,
        MoveToPlate,
        FinishMove
    }
    public class FoodMove : MonoBehaviour
    {
        #region Editor fields
        [Header("Параметры движения еды")]
        [SerializeField] private FoodMoveParams foodMoveParams;
        [Header("Иконка еды еды")]
        [SerializeField] private Image icon;      
        
              
        #endregion

        #region private variables
        private FoodState currentState = FoodState.None;
        private RectTransform rectTransform = null;
        private Vector2 randomPos = Vector2.zero;
        private Vector2 platePos;
        private Rect fieldRect;
        private List<GridCellContainer> wayPoints = new List<GridCellContainer>();
        private List<GridCellContainer> way = new List<GridCellContainer>();
        private GridCellContainer endPoint, curPosition, nextPosition, finishPoint;
        private float spendedTime = 0;



        #endregion

        #region public variables
        public System.Action MoveEnded = null;
        public int CellSize { get { return foodMoveParams.cellSpace; } }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            rectTransform = (RectTransform)transform;
            fieldRect = ((RectTransform)rectTransform.parent).rect;
            
        }
        private void Start()
        {            
            
        }
        private void Update()
        {

            switch(currentState)
            {
                case FoodState.MoveToPlate:
                    MoveToPlate(Time.deltaTime);
                    break;
                case FoodState.RandomMove:
                    spendedTime += Time.deltaTime;
                    
                    if(spendedTime> foodMoveParams.maxMovingTime)
                    {
                        currentState = FoodState.None;
                        StartMoveToPlate();
                    }
                    else RandomMove(Time.deltaTime);
                    break;
                case FoodState.FinishMove:
                    rectTransform.anchoredPosition = Vector2.MoveTowards(rectTransform.anchoredPosition, platePos, Time.deltaTime * foodMoveParams.moveSpeed);
                    if (Vector2.Distance(rectTransform.anchoredPosition, platePos) < 0.1f)
                    {
                        currentState = FoodState.None;
                        MoveEnded?.Invoke();
                    }
                    break;
            }
        }
        private void OnDrawGizmos()
        {
            //Gizmos.color = Color.red;
            //if (endPoint != null) Gizmos.DrawSphere(endPoint.position, 0.5f);
            //Gizmos.color = Color.gray;
            //if (finishPoint != null) Gizmos.DrawSphere(finishPoint.position, 0.3f);
            //Gizmos.color = Color.red;
            //for (int i = 0; i < wayPoints.Count; i++) Gizmos.DrawSphere(wayPoints[i].position, 0.3f);
            //for (int i = 0; i < way.Count; i++) Gizmos.DrawSphere(way[i].position, 0.1f);
           // Gizmos.color = Color.gray;
        }
        #endregion

        #region Private Methods
        

        private void StartRandomMove()
        {            
            SelectNextWayPoint();           
            
        }
        private void SelectNextWayPoint()
        {
            if(wayPoints.Count>0)
            {
                finishPoint = wayPoints[0];
                wayPoints.RemoveAt(0);                
                UpdateWay();
            }
            else
            {                
                currentState = FoodState.MoveToPlate;
            }
            
        }
        private void UpdateWay()
        {
            
            way = HideAndSeek.Instance.FindWay(curPosition.gridPosition, finishPoint.gridPosition);
            
            if (way.Count > 0)
            {
                way.RemoveAt(0);
                nextPosition = way[0];
                way.RemoveAt(0);
                HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, false);
                currentState = FoodState.RandomMove;
            }
            else
            {
                currentState = FoodState.None;               
                StartMoveToPlate();
            }
            
        }
        private void UpdateWayToEnd()
        {           
            way = HideAndSeek.Instance.FindWay(curPosition.gridPosition, endPoint.gridPosition);            
            if (way.Count > 0)
            {
                way.RemoveAt(0);
                nextPosition = way[0];
                way.RemoveAt(0);
                HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, false);
                currentState = FoodState.MoveToPlate;
            }
            else
            {
                /*rectTransform.anchoredPosition = platePos;
                HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, false);
                MoveEnded?.Invoke();*/
                Invoke("UpdateWayToEnd", 0.5f);                
            }

        }
        private void MoveToPlate(float deltaTime)
        {

            rectTransform.anchoredPosition = Vector2.MoveTowards(rectTransform.anchoredPosition, HideAndSeek.Instance.WorldToScreen(nextPosition.position), deltaTime * foodMoveParams.moveSpeed);
            if (Vector2.Distance(rectTransform.anchoredPosition, HideAndSeek.Instance.WorldToScreen(nextPosition.position)) < 0.1f)
            {
                rectTransform.anchoredPosition = HideAndSeek.Instance.WorldToScreen(nextPosition.position);
                //HideAndSeek.Instance.SetCellEmpty(curPosition.gridPosition);
                HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, true);
                curPosition = nextPosition;

                //HideAndSeek.Instance.SetCellOccupied(curPosition.gridPosition);
                if (nextPosition == finishPoint)
                {
                    currentState = FoodState.FinishMove;
                    //rectTransform.anchoredPosition = platePos;
                    HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, false);
                    //MoveEnded?.Invoke();
                }
                else
                {
                    currentState = FoodState.None;
                    UpdateWayToEnd();
                }
            }
        }
        private void StartMoveToPlate()
        {
            finishPoint = endPoint;
            HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, true);
            UpdateWayToEnd();
        }
        private void RandomMove(float deltaTime)
        {
            
            rectTransform.anchoredPosition = Vector2.MoveTowards(rectTransform.anchoredPosition, HideAndSeek.Instance.WorldToScreen(nextPosition.position), deltaTime * foodMoveParams.moveSpeed);
            if(Vector2.Distance(rectTransform.anchoredPosition, HideAndSeek.Instance.WorldToScreen(nextPosition.position))<0.1f)
            {
                rectTransform.anchoredPosition = HideAndSeek.Instance.WorldToScreen(nextPosition.position);
                //HideAndSeek.Instance.SetCellEmpty(curPosition.gridPosition);
                HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, true);
                curPosition = nextPosition;
                //HideAndSeek.Instance.SetCellOccupied(curPosition.gridPosition);
                if (nextPosition == finishPoint)
                {
                    currentState = FoodState.None;                    
                    SelectNextWayPoint();
                }
                else
                {
                    currentState = FoodState.None;
                    UpdateWay();
                }
            }
        }
        #endregion

        #region Public Methods
        public void SetIconSprite(Sprite sp)
        {
            if(icon)
            {
                icon.sprite = sp;
            }
        }
        public void SetPosition(GridCellContainer pos)
        {
            if(rectTransform)
            {                
                rectTransform.position = HideAndSeek.Instance.WorldToScreen(pos.position);                
                curPosition = pos;
                //HideAndSeek.Instance.SetPlacelWalkable(curPosition.gridPosition, foodMoveParams.cellSpace, foodMoveParams.cellSpace, false);
                //HideAndSeek.Instance.SetCellOccupied(curPosition.gridPosition);
            }
        }
        public void SetPlate(GridCellContainer cell, Vector2 pos)
        {
            platePos = pos;
            endPoint = cell;
            
        }
        public void AddWayPoint(GridCellContainer cell)
        {
            wayPoints.Add(cell);
        }
        public void StartMove()
        {
            StartRandomMove();
        }
        #endregion
    }
}