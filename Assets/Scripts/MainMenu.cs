﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    public void OnHideAndSeekBtn()
    {
        SceneManager.LoadScene(1);
    }

    public void OnDanceBtn()
    {
        SceneManager.LoadScene(2);
    }   
    public void OnExitGame()
    {
        Application.Quit();
    }

    public void OnFindBtn()
    {
        SceneManager.LoadScene(3);
    }

    public void OnQuestBtn()
    {
        SceneManager.LoadScene(4);
    }

    public void OnReceiptBtn()
    {
        SceneManager.LoadScene(5);
    }
}
