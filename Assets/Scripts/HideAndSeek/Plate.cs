﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace HideAndSeekScene
{
    public class Plate : MonoBehaviour
    {

        #region Editor fields
        [SerializeField]
        private GameObject icon;
        [SerializeField]
        private GameObject startPosObject;
        [SerializeField]
        private float moveSpeed;
        #endregion

        #region private variables
        private RectTransform iconTransform;
        private Vector2 startPos;
        private bool canMove = false;
        private Vector2 movePoint = Vector2.zero;
        #endregion

        #region public variables
        public RectTransform rectTransform { get; private set; }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            iconTransform = (RectTransform)icon.transform;
            rectTransform = (RectTransform)transform;
            startPos = ((RectTransform)startPosObject.transform).anchoredPosition;
            SetStartPos();
        }
        private void Start()
        {

        }
        private void Update()
        {
            if (canMove)
            {
                iconTransform.anchoredPosition = Vector2.MoveTowards(iconTransform.anchoredPosition, movePoint, Time.deltaTime * moveSpeed);
                if (Vector2.Distance(iconTransform.anchoredPosition, movePoint) < 0.1f)
                {
                    iconTransform.anchoredPosition = movePoint;
                    canMove = false;
                }
            }

        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        public void MoveToStart()
        {
            movePoint = startPos;
            canMove = true;
        }
        public void MoveToCenter()
        {
            movePoint = Vector2.zero;
            canMove = true;
        }
        public void SetStartPos()
        {
            iconTransform.anchoredPosition = startPos;
        }
        #endregion






    }
}