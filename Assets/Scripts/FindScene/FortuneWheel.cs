﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FindScene
{
    [System.Serializable]
    public struct FortuneWheelParams
    {
        public float startRotarionSpeed;
        public float speedStep;
        public float exitSpeed;
        public int xIndex;
        public int direction;
        public int wheelID;
    }
    public enum WheelState
    {
        Stopped,
        Moving,
        EndMoving
    }
    public class FortuneWheel : MonoBehaviour
    {
        #region Editor fields
        [SerializeField]
        private int segmentsCount;
        [SerializeField]
        private FortuneWheelParams wheelParams;
        #endregion

        #region private variables
        private WheelState currentState = WheelState.Stopped;
        private RectTransform rectTransform;
        private float curWheelSpeed = 0;
        private float segmentAngle = 0, endAngle;
        private Quaternion endEuler;
        private bool canMove = false;
        private int currentSeg;
        #endregion

        #region public variables
        public System.Action<int> wheelEnded = null;
        #endregion

        #region Unity Methods
        private void Awake()
        {
            rectTransform = (RectTransform)transform;
            endEuler = rectTransform.localRotation;
            curWheelSpeed = wheelParams.startRotarionSpeed;
            segmentAngle = 360.0f / (float)segmentsCount;
        }
        private void Start()
        {
            currentState = WheelState.Stopped;
        }
        private void Update()
        {
            if (!canMove) return;
            switch (currentState)
            {
                case WheelState.Stopped:
                    return;

                case WheelState.Moving:
                    RotateWheel(Time.deltaTime);
                    break;
                case WheelState.EndMoving:
                    RotateToFinish(Time.deltaTime);
                    break;
            }
        }
        #endregion

        #region Private Methods
        private void RotateWheel(float deltaTime)
        {
            if (rectTransform)
            {
                curWheelSpeed -= wheelParams.speedStep;
                if (curWheelSpeed > wheelParams.exitSpeed)
                {
                    Quaternion rotation = rectTransform.localRotation;
                    rotation.eulerAngles = new Vector3(0, 0, rotation.eulerAngles.z + curWheelSpeed * deltaTime* wheelParams.direction);
                    rectTransform.localRotation = rotation;
                }
                else
                {
                    canMove = false;
                    FinishRotation();
                }
            }
        }
        private void FinishRotation()
        {
            currentSeg = Mathf.RoundToInt(rectTransform.eulerAngles.z / segmentAngle);
            Debug.Log(currentSeg);
            endAngle = currentSeg * segmentAngle;
            endEuler = Quaternion.Euler(0, 0, endAngle);
            canMove = true;
            currentState = WheelState.EndMoving;
        }
        private void RotateToFinish(float deltaTime)
        {
            Quaternion rotation = rectTransform.localRotation;            
            float angle = Mathf.Abs(Mathf.Abs(endAngle) - Mathf.Abs(rotation.eulerAngles.z));

            if (angle < 10f)
            {
                rotation.eulerAngles = new Vector3(0, 0, rotation.eulerAngles.z + wheelParams.exitSpeed * deltaTime);
                rectTransform.localRotation = rotation;
            }
            else
            {
                currentState = WheelState.Stopped;
                wheelEnded?.Invoke(wheelParams.wheelID);
            }


        }

        #endregion

        #region Public Methods
        public bool IsCurrentX()
        {
            return currentSeg == wheelParams.xIndex;
        }
        public void RotateWheel()
        {
            canMove = true;
            int dir = UnityEngine.Random.Range(0, 2) > 0 ? -1 : 1;
            curWheelSpeed = wheelParams.startRotarionSpeed + UnityEngine.Random.Range(0, wheelParams.startRotarionSpeed / 4) * dir;
            currentState = WheelState.Moving;
        }
        #endregion
    }
}

