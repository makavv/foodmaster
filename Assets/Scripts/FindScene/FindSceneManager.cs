﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace FindScene
{
    public enum LevelDiffucult
    {
        Low,
        Medium,
        Hard
    }
    public class FindSceneManager : MonoBehaviour
    {
        #region Editor fields
        [SerializeField] private GameObject leftNo, rightNo, or, and;
        [SerializeField] private FortuneWheel leftWheel, rightWheel;
        [SerializeField] private Image lowBtnIcon, mediumBtnIcon, hightBtnIcon;
        [SerializeField] private Color selectedColor;
        #endregion

        #region private variables
        private LevelDiffucult currentDifficult = LevelDiffucult.Low;
        private int wheelEndedCount = 0;
        #endregion

        #region public variables

        #endregion

        #region Unity Methods
        private void Awake()
        {
            leftWheel.wheelEnded = OnWheelEnded;
            rightWheel.wheelEnded = OnWheelEnded;
        }
        private void Start()
        {
            HideStates();
            OnLow();
        }
        #endregion

        #region Private Methods
        
        private void ShowStates()
        {
            Debug.Log("ShowStates");
            and.SetActive(true);
            if (currentDifficult == LevelDiffucult.Low) return;
            int rnd = UnityEngine.Random.Range(0, 100);
            if (rnd < 25)
            {
                leftNo.SetActive(false);
                rightNo.SetActive(false);
            }
            else if (rnd > 25 && rnd < 50)
            {
                leftNo.SetActive(true);
                rightNo.SetActive(false);
            }
            else if (rnd > 50 && rnd < 75)
            {
                leftNo.SetActive(false);
                rightNo.SetActive(true);
            }
            else if (rnd > 75 && rnd < 100)
            {
                leftNo.SetActive(true);
                rightNo.SetActive(true);
            }

            if (leftWheel.IsCurrentX())
            {
                leftNo.SetActive(false);
            }

            if (rightWheel.IsCurrentX())
            {
                rightNo.SetActive(false);
            }
            if (currentDifficult == LevelDiffucult.Medium) return;
            rnd = UnityEngine.Random.Range(0, 100);
            and.SetActive(rnd>50);
            or.SetActive(rnd<=50);

            if(leftWheel.IsCurrentX() && rightWheel.IsCurrentX())
            {
                and.SetActive(true);
            }
            
        }        
        private void OnWheelEnded(int index)
        {
            wheelEndedCount++;
            if(wheelEndedCount==2)
            {
                wheelEndedCount = 0;
                ShowStates();
            }
        }
        #endregion

        #region Public Methods
        public void GoMenu()
        {
            SceneManager.LoadScene(0);
        }
        public void HideStates()
        {
            or.SetActive(false);
            and.SetActive(false);
            leftNo.SetActive(false);
            rightNo.SetActive(false);
        }
        public void OnLow()
        {
            currentDifficult = LevelDiffucult.Low;
            lowBtnIcon.color = selectedColor;
            mediumBtnIcon.color = Color.white;
            hightBtnIcon.color = Color.white;
        }

        public void OnMedium()
        {
            currentDifficult = LevelDiffucult.Medium;
            lowBtnIcon.color = Color.white;
            mediumBtnIcon.color = selectedColor;
            hightBtnIcon.color = Color.white;
        }

        public void OnHard()
        {
            currentDifficult = LevelDiffucult.Hard;
            lowBtnIcon.color = Color.white;
            mediumBtnIcon.color = Color.white;
            hightBtnIcon.color = selectedColor;
        }
        #endregion
    }
}
