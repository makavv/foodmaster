﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace QuestScene
{
    public class QuestSceneManager : MonoBehaviour
    {
        #region Editor fields
        [Header("Список загадок")]
        [SerializeField] private List<Quest> quests = new List<Quest>();
        [SerializeField] private Text questTextField;
        [Header("Задержка между появлением букв")]
        [SerializeField] private float animationDelay = 0.5f;

        [SerializeField] private Image answerIcon;
        [SerializeField] private Text answerText;
        [SerializeField] private GameObject answerPanel;
        #endregion

        #region private variables
        private List<int> availableQuestIndexes = new List<int>();
        private Quest currentQuest = null;
        #endregion

        #region public variables

        #endregion

        #region Unity Methods
        private void Awake()
        {
            CreateQuestList();
        }
        private void Start()
        {
            SelectNextQuest();
        }
        #endregion

        #region Private Methods
        private void CreateQuestList()
        {
            availableQuestIndexes.Clear();
            for (int i = 0; i < quests.Count; i++) availableQuestIndexes.Add(i);
        }

        private bool SelectRandomIndex(List<int> list, out int index)
        {
            index = -1;
            if (list.Count > 0)
            {
                int id = UnityEngine.Random.Range(0, list.Count);
                index = list[id];
                return true;
            }
            return false;
        }
        private IEnumerator PlayQuestAnimation()
        {
            int n = currentQuest.questText.Length;
            
            for(int i=0;i<n;i++)
            {
                if (questTextField)
                {
                    if(i<(n-1)) questTextField.text = currentQuest.questText.Remove(i);
                    else questTextField.text = currentQuest.questText;
                }
                if(currentQuest.questText[i]==' ') yield return new WaitForSeconds(animationDelay/50);
                else yield return new WaitForSeconds(animationDelay);
            }
           
        }
        #endregion

        #region Public Methods
        public void GoMenu()
        {
            SceneManager.LoadScene(0);
        }
        public void SelectNextQuest()
        {
            if (answerPanel)
            {
                answerPanel.SetActive(false);
            }
            int questID = -1;
            if (availableQuestIndexes.Count > 0)
            {
                if (SelectRandomIndex(availableQuestIndexes, out questID))
                {
                    if (questID >= 0)
                    {
                        currentQuest = quests[questID];
                        availableQuestIndexes.Remove(questID);
                        StartCoroutine(PlayQuestAnimation());
                    }
                }
            }
            else
            {
                CreateQuestList();
                SelectNextQuest();
            }
        }
        public void ShowAnswer()
        {
            if(answerPanel)
            {
                answerPanel.SetActive(true);
                if (answerIcon) answerIcon.sprite = currentQuest.answerIcon;
                if (answerText) answerText.text = currentQuest.answerText;
            }
        }
        #endregion
    }
}