﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReceiptScene
{

    [CreateAssetMenu(fileName = "New Receipt", menuName = "Receipt data", order = 51)]
    public class Receipt : ScriptableObject
    {
        public string receiptName;
        public List<Ingridient> ingridients = new List<Ingridient>();
    }
    [System.Serializable]
    public class Ingridient
    {
        public string name;
        public Sprite icon;
    }
}